﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;


namespace StoryNodeCreator {
    public partial class Form1 : Form {


        DataSet dSet = new DataSet();
        List<StoryNode> Nodes = new List<StoryNode>();

        List<string> NodeNames = new List<string>();


        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            resetAll();
        }




        private void saveToolStripMenuItem_Click(object sender, EventArgs e) {

        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e) {

        }

        private void textBox3_TextChanged(object sender, EventArgs e) {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e) {
            
        }






        private void b_SaveNode_Click(object sender, EventArgs e) {
            SaveNode();
            updateList();
        }

        void SaveNode() {
            if (Nodes != null && Nodes.Count != 0) {
                int i = 0;
                foreach (var node in Nodes) {
                    if (node.m_Key == tB_NodeKey.Text) {
                        tB_Console.Text += "Found Node With The Same Name:: Updating" + "\r\n";
                        updateNode(i);
                        return;
                    }
                    ++i;
                }
                AddNode();
            }
            else {
                AddNode();
            }
        }

        void FillFromDataset(string Table) {



        }

        void newNode() {
            Nodes.Add(new StoryNode(tB_NodeKey.Text));
            Nodes.Last().update(
                tB_NodeKey.Text,
                tB_TalkingTo.Text,
                tB_MainText.Text,
                tB_Option1.Text,
                tB_Option1Links.Text,
                (int)nUD_Option1Mod.Value,
                tB_Option2.Text,
                tB_Option2Links.Text,
                (int)nUD_Option2Mod.Value,
                tB_Option3.Text,
                tB_Option3Links.Text,
                (int)nUD_Option3Mod.Value
                );
        }
        void updateNode(int i) {
            Nodes[i].update(
                tB_NodeKey.Text,
                tB_TalkingTo.Text,
                tB_MainText.Text,
                tB_Option1.Text,
                tB_Option1Links.Text,
                (int)nUD_Option1Mod.Value,
                tB_Option2.Text,
                tB_Option2Links.Text,
                (int)nUD_Option2Mod.Value,
                tB_Option3.Text,
                tB_Option3Links.Text,
                (int)nUD_Option3Mod.Value
                );
            tB_Console.Text += "Node: " + tB_NodeKey.Text + " Has been Updated" + "\r\n";
        }
        void AddNode() {
            StoryNode Node = new StoryNode(tB_NodeKey.Text);
            Node.update(
                tB_NodeKey.Text,
                tB_TalkingTo.Text,
                tB_MainText.Text,
                tB_Option1.Text,
                tB_Option1Links.Text,
                (int)nUD_Option1Mod.Value,
                tB_Option2.Text,
                tB_Option2Links.Text,
                (int)nUD_Option2Mod.Value,
                tB_Option3.Text,
                tB_Option3Links.Text,
                (int)nUD_Option3Mod.Value
            );
            Nodes.Add(Node);
            tB_Console.Text += "New Node Added: " + tB_NodeKey.Text + "\r\n";
        }



        void LoadNode(int i) {
            tB_NodeKey.Text = Nodes[i].m_Node.TableName;
            tB_TalkingTo.Text = Nodes[i].m_Node.Rows[0]["TalkingTo"].ToString();
            tB_MainText.Text = Nodes[i].m_Node.Rows[0]["MainText"].ToString();
            tB_Option1.Text = Nodes[i].m_Node.Rows[0]["Option1"].ToString();
            tB_Option1Links.Text = Nodes[i].m_Node.Rows[0]["Option1Link"].ToString();
            nUD_Option1Mod.Value = Int32.Parse(Nodes[i].m_Node.Rows[0]["Option1Mod"].ToString());
            tB_Option2.Text = Nodes[i].m_Node.Rows[0]["Option2"].ToString();
            tB_Option2Links.Text = Nodes[i].m_Node.Rows[0]["Option2Link"].ToString();
            nUD_Option2Mod.Value = Int32.Parse(Nodes[i].m_Node.Rows[0]["Option2Mod"].ToString());
            tB_Option3.Text = Nodes[i].m_Node.Rows[0]["Option3"].ToString();
            tB_Option3Links.Text = Nodes[i].m_Node.Rows[0]["Option3Link"].ToString();
            nUD_Option3Mod.Value = Int32.Parse(Nodes[i].m_Node.Rows[0]["Option3Mod"].ToString());

            tB_Console.Text += "Node: " + Nodes[i].m_Node.TableName + " has been loaded" + "\r\n";
        }

        void updateList() {
            NodeNames.Clear();
            lB_Nodes.DataSource = null;


            foreach (var Node in Nodes) {
                NodeNames.Add(Node.m_Key);
            }
            lB_Nodes.DataSource = NodeNames;
            this.Update();

            tB_Console.Text += "List of all nodes has been updated" + "\r\n";

        }

        private void b_Update_Click(object sender, EventArgs e) {
            updateList();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) {

        }

        private void button2_Click(object sender, EventArgs e) {
            tB_Console.Clear();
        }

        private void button3_Click(object sender, EventArgs e) {
            Clipboard.SetText(lB_Nodes.SelectedValue.ToString());
        }

        private void button1_Click(object sender, EventArgs e) {
            var confirmResult = MessageBox.Show("Would you like to save node before continuing",
                                         "Changing Node",
                         MessageBoxButtons.YesNoCancel);
            if (confirmResult == DialogResult.Yes) {
                SaveNode();
                updateList();
            }
            else if (confirmResult == DialogResult.No) {

            }
            else if (confirmResult == DialogResult.Cancel) {
                return;
            }
            LoadNode(lB_Nodes.SelectedIndex);
        }

        private void savingLoadingToolStripMenuItem_Click(object sender, EventArgs e) {

        }

        private void saveAllToolStripMenuItem_Click(object sender, EventArgs e) {
            dSet.Reset();
            foreach (var Node in Nodes) {
                dSet.Tables.Add(Node.m_Node.Copy());
            }
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "XML|*.xml";
            if (sfd.ShowDialog() == DialogResult.OK) {
                dSet.WriteXml(sfd.FileName);   
            }
        }

        private void b_NewNode_Click(object sender, EventArgs e) {

        }

        private void button4_Click(object sender, EventArgs e) {


            if (Nodes.Count != 0) {
                var confirmResult = MessageBox.Show("Are you sure you want to remove Node: " + lB_Nodes.SelectedValue.ToString(),
                 "Remove Node!",
                    MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes) {
                    Nodes.RemoveAt(lB_Nodes.SelectedIndex);

                    tB_Console.Text += "Node Removed" + "\r\n";
                    updateList();
                }
                if (confirmResult == DialogResult.No) {
                    return;
                }
            }
        }

        private void loadAllToolStripMenuItem_Click(object sender, EventArgs e) {
            var confirmResult = MessageBox.Show("Warning Loading a new NodeTree will Wipe current work" + "\r\n" + "Would you like to continue",
                             "Loading new NodeTree",
             MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes) {

                OpenFileDialog lfd = new OpenFileDialog();
                lfd.Filter = "XML|*.xml";
                if (lfd.ShowDialog() == DialogResult.OK) {
                    XmlReader xmlFile = XmlReader.Create(lfd.FileName, new XmlReaderSettings());

                    //dSet.Reset();
                    dSet.ReadXml(lfd.FileName);
                    resetAll();
                    Nodes.Clear();
                    foreach (DataTable table in dSet.Tables) {
                        Nodes.Add(new StoryNode(table));
                    }
                    updateList();
                }
                else if (confirmResult == DialogResult.No) {
                    return;
                }
            }
        }


        private void resetAll() {
            tB_NodeKey.Text = "null";
            tB_TalkingTo.Text = "null";
            tB_MainText.Text = "null";
            tB_Option1.Text = "null";
            tB_Option2.Text = "null";
            tB_Option3.Text = "null";
            tB_Option1Links.Text = "null";
            tB_Option2Links.Text = "null";
            tB_Option3Links.Text = "null";
            nUD_Option1Mod.Value = 0;
            nUD_Option2Mod.Value = 0;
            nUD_Option3Mod.Value = 0;
            lB_Nodes.DataSource = null;
            this.Refresh();
        }

        private void createNewToolStripMenuItem_Click(object sender, EventArgs e) {
            var confirmResult = MessageBox.Show("Would you like to save all before continuing",
                                         "New Tree",
                         MessageBoxButtons.YesNoCancel);
            if (confirmResult == DialogResult.Yes) {
                dSet.Reset();
                foreach (var Node in Nodes) {
                    dSet.Tables.Add(Node.m_Node.Copy());
                }
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "XML|*.xml";
                if (sfd.ShowDialog() == DialogResult.OK) {
                    dSet.WriteXml(sfd.FileName);
                }
            }
            else if (confirmResult == DialogResult.No) {

            }
            else if (confirmResult == DialogResult.Cancel) {
                return;
            }

            resetAll();


        }
    }
}
