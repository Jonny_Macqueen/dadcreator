﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Data;

namespace StoryNodeCreator {
    public class StoryNode {
        public string m_Key;
        public DataTable m_Node;
        Form1 f1;

        public StoryNode(DataTable table) {
            m_Key = table.TableName;
            m_Node = table;
        }

        public StoryNode(string key) {
            m_Key = key;
            m_Node = new DataTable(key);


            m_Node.Columns.Add("TalkingTo", typeof(string));
            m_Node.Columns.Add("MainText", typeof(string));
            m_Node.Columns.Add("Option1", typeof(string));
            m_Node.Columns.Add("Option1Link", typeof(string));
            m_Node.Columns.Add("Option1Mod", typeof(string));
            m_Node.Columns.Add("Option2", typeof(string));
            m_Node.Columns.Add("Option2Link", typeof(string));
            m_Node.Columns.Add("Option2Mod", typeof(string));
            m_Node.Columns.Add("Option3", typeof(string));
            m_Node.Columns.Add("Option3Link", typeof(string));
            m_Node.Columns.Add("Option3Mod", typeof(string));
        }





        ~StoryNode() { }

        public void update(
            string key,
            string TalkingTo,
            string MainText,
            string Option1, string Option1Link, int Option1Mod,
            string Option2, string Option2Link, int Option2Mod,
            string Option3, string Option3Link, int Option3Mod
            ) {

            m_Key = key;
            m_Node.Clear();


            m_Node.Rows.Add(
             TalkingTo,
             MainText,
             Option1, Option1Link, Option1Mod,
             Option2, Option2Link, Option2Mod,
             Option3, Option3Link, Option3Mod
            );
        }

    }
}
