﻿namespace StoryNodeCreator {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.tB_MainText = new System.Windows.Forms.TextBox();
            this.tB_NodeKey = new System.Windows.Forms.TextBox();
            this.tB_Option1 = new System.Windows.Forms.TextBox();
            this.tB_Option2 = new System.Windows.Forms.TextBox();
            this.tB_Option3 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.tB_TalkingTo = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.nUD_Option1Mod = new System.Windows.Forms.NumericUpDown();
            this.nUD_Option2Mod = new System.Windows.Forms.NumericUpDown();
            this.nUD_Option3Mod = new System.Windows.Forms.NumericUpDown();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createNewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savingLoadingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lB_Nodes = new System.Windows.Forms.ListBox();
            this.b_SaveNode = new System.Windows.Forms.Button();
            this.b_Update = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tB_Console = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.tB_Option1Links = new System.Windows.Forms.TextBox();
            this.tB_Option2Links = new System.Windows.Forms.TextBox();
            this.tB_Option3Links = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_Option1Mod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_Option2Mod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_Option3Mod)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tB_MainText
            // 
            this.tB_MainText.Location = new System.Drawing.Point(12, 86);
            this.tB_MainText.Multiline = true;
            this.tB_MainText.Name = "tB_MainText";
            this.tB_MainText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tB_MainText.Size = new System.Drawing.Size(521, 166);
            this.tB_MainText.TabIndex = 0;
            this.tB_MainText.Text = "NaN";
            // 
            // tB_NodeKey
            // 
            this.tB_NodeKey.Location = new System.Drawing.Point(12, 44);
            this.tB_NodeKey.Name = "tB_NodeKey";
            this.tB_NodeKey.Size = new System.Drawing.Size(100, 20);
            this.tB_NodeKey.TabIndex = 1;
            this.tB_NodeKey.Text = "NaN";
            // 
            // tB_Option1
            // 
            this.tB_Option1.Location = new System.Drawing.Point(12, 307);
            this.tB_Option1.Name = "tB_Option1";
            this.tB_Option1.Size = new System.Drawing.Size(379, 20);
            this.tB_Option1.TabIndex = 2;
            this.tB_Option1.Text = "NaN";
            this.tB_Option1.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // tB_Option2
            // 
            this.tB_Option2.Location = new System.Drawing.Point(12, 350);
            this.tB_Option2.Name = "tB_Option2";
            this.tB_Option2.Size = new System.Drawing.Size(379, 20);
            this.tB_Option2.TabIndex = 3;
            this.tB_Option2.Text = "NaN";
            // 
            // tB_Option3
            // 
            this.tB_Option3.Location = new System.Drawing.Point(12, 396);
            this.tB_Option3.Name = "tB_Option3";
            this.tB_Option3.Size = new System.Drawing.Size(379, 20);
            this.tB_Option3.TabIndex = 4;
            this.tB_Option3.Text = "NaN";
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.Location = new System.Drawing.Point(12, 27);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 13);
            this.textBox6.TabIndex = 5;
            this.textBox6.Text = "Node Key";
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.Location = new System.Drawing.Point(12, 70);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(100, 13);
            this.textBox7.TabIndex = 6;
            this.textBox7.Text = "Main Text";
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox8.Location = new System.Drawing.Point(12, 288);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(100, 13);
            this.textBox8.TabIndex = 7;
            this.textBox8.Text = "Option 1";
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox9.Location = new System.Drawing.Point(12, 333);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(100, 13);
            this.textBox9.TabIndex = 8;
            this.textBox9.Text = "Option 2";
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox10.Location = new System.Drawing.Point(12, 377);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(100, 13);
            this.textBox10.TabIndex = 9;
            this.textBox10.Text = "Option 3";
            // 
            // tB_TalkingTo
            // 
            this.tB_TalkingTo.Location = new System.Drawing.Point(127, 44);
            this.tB_TalkingTo.Name = "tB_TalkingTo";
            this.tB_TalkingTo.Size = new System.Drawing.Size(100, 20);
            this.tB_TalkingTo.TabIndex = 22;
            this.tB_TalkingTo.Text = "NaN";
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox24.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox24.Location = new System.Drawing.Point(127, 25);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(100, 13);
            this.textBox24.TabIndex = 23;
            this.textBox24.Text = "Talking To";
            // 
            // textBox25
            // 
            this.textBox25.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox25.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox25.Location = new System.Drawing.Point(12, 269);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(100, 13);
            this.textBox25.TabIndex = 24;
            this.textBox25.Text = "Chat Options";
            // 
            // textBox26
            // 
            this.textBox26.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox26.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox26.Location = new System.Drawing.Point(397, 272);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(65, 13);
            this.textBox26.TabIndex = 25;
            this.textBox26.Text = "Node Links";
            // 
            // nUD_Option1Mod
            // 
            this.nUD_Option1Mod.Location = new System.Drawing.Point(468, 307);
            this.nUD_Option1Mod.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.nUD_Option1Mod.Name = "nUD_Option1Mod";
            this.nUD_Option1Mod.Size = new System.Drawing.Size(56, 20);
            this.nUD_Option1Mod.TabIndex = 26;
            // 
            // nUD_Option2Mod
            // 
            this.nUD_Option2Mod.Location = new System.Drawing.Point(468, 351);
            this.nUD_Option2Mod.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.nUD_Option2Mod.Name = "nUD_Option2Mod";
            this.nUD_Option2Mod.Size = new System.Drawing.Size(56, 20);
            this.nUD_Option2Mod.TabIndex = 27;
            // 
            // nUD_Option3Mod
            // 
            this.nUD_Option3Mod.Location = new System.Drawing.Point(468, 397);
            this.nUD_Option3Mod.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.nUD_Option3Mod.Name = "nUD_Option3Mod";
            this.nUD_Option3Mod.Size = new System.Drawing.Size(56, 20);
            this.nUD_Option3Mod.TabIndex = 28;
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox11.Location = new System.Drawing.Point(468, 272);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(66, 13);
            this.textBox11.TabIndex = 29;
            this.textBox11.Text = "Like Mod";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainToolStripMenuItem,
            this.savingLoadingToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(926, 24);
            this.menuStrip1.TabIndex = 30;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mainToolStripMenuItem
            // 
            this.mainToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createNewToolStripMenuItem});
            this.mainToolStripMenuItem.Name = "mainToolStripMenuItem";
            this.mainToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.mainToolStripMenuItem.Text = "Main";
            // 
            // createNewToolStripMenuItem
            // 
            this.createNewToolStripMenuItem.Name = "createNewToolStripMenuItem";
            this.createNewToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.createNewToolStripMenuItem.Text = "Create New";
            this.createNewToolStripMenuItem.Click += new System.EventHandler(this.createNewToolStripMenuItem_Click);
            // 
            // savingLoadingToolStripMenuItem
            // 
            this.savingLoadingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveAllToolStripMenuItem,
            this.loadAllToolStripMenuItem});
            this.savingLoadingToolStripMenuItem.Name = "savingLoadingToolStripMenuItem";
            this.savingLoadingToolStripMenuItem.Size = new System.Drawing.Size(102, 20);
            this.savingLoadingToolStripMenuItem.Text = "Saving/Loading";
            this.savingLoadingToolStripMenuItem.Click += new System.EventHandler(this.savingLoadingToolStripMenuItem_Click);
            // 
            // saveAllToolStripMenuItem
            // 
            this.saveAllToolStripMenuItem.Name = "saveAllToolStripMenuItem";
            this.saveAllToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveAllToolStripMenuItem.Text = "Save";
            this.saveAllToolStripMenuItem.Click += new System.EventHandler(this.saveAllToolStripMenuItem_Click);
            // 
            // loadAllToolStripMenuItem
            // 
            this.loadAllToolStripMenuItem.Name = "loadAllToolStripMenuItem";
            this.loadAllToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.loadAllToolStripMenuItem.Text = "Load";
            this.loadAllToolStripMenuItem.Click += new System.EventHandler(this.loadAllToolStripMenuItem_Click);
            // 
            // lB_Nodes
            // 
            this.lB_Nodes.FormattingEnabled = true;
            this.lB_Nodes.Location = new System.Drawing.Point(539, 86);
            this.lB_Nodes.Name = "lB_Nodes";
            this.lB_Nodes.Size = new System.Drawing.Size(195, 173);
            this.lB_Nodes.TabIndex = 32;
            this.lB_Nodes.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // b_SaveNode
            // 
            this.b_SaveNode.Location = new System.Drawing.Point(539, 262);
            this.b_SaveNode.Name = "b_SaveNode";
            this.b_SaveNode.Size = new System.Drawing.Size(75, 23);
            this.b_SaveNode.TabIndex = 33;
            this.b_SaveNode.Text = "Add/Update";
            this.b_SaveNode.UseVisualStyleBackColor = true;
            this.b_SaveNode.Click += new System.EventHandler(this.b_SaveNode_Click);
            // 
            // b_Update
            // 
            this.b_Update.Location = new System.Drawing.Point(577, 62);
            this.b_Update.Name = "b_Update";
            this.b_Update.Size = new System.Drawing.Size(75, 23);
            this.b_Update.TabIndex = 35;
            this.b_Update.Text = "Update";
            this.b_Update.UseVisualStyleBackColor = true;
            this.b_Update.Click += new System.EventHandler(this.b_Update_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(539, 67);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(32, 13);
            this.textBox1.TabIndex = 36;
            this.textBox1.Text = "Nodes";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(632, 262);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 23);
            this.button1.TabIndex = 37;
            this.button1.Text = "Change Node";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tB_Console
            // 
            this.tB_Console.BackColor = System.Drawing.SystemColors.MenuText;
            this.tB_Console.ForeColor = System.Drawing.Color.DarkRed;
            this.tB_Console.Location = new System.Drawing.Point(736, 86);
            this.tB_Console.Multiline = true;
            this.tB_Console.Name = "tB_Console";
            this.tB_Console.Size = new System.Drawing.Size(187, 263);
            this.tB_Console.TabIndex = 41;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(736, 355);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 42;
            this.button2.Text = "Clear";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tB_Option1Links
            // 
            this.tB_Option1Links.Location = new System.Drawing.Point(397, 306);
            this.tB_Option1Links.Name = "tB_Option1Links";
            this.tB_Option1Links.Size = new System.Drawing.Size(65, 20);
            this.tB_Option1Links.TabIndex = 43;
            this.tB_Option1Links.Text = "NaN";
            // 
            // tB_Option2Links
            // 
            this.tB_Option2Links.Location = new System.Drawing.Point(397, 349);
            this.tB_Option2Links.Name = "tB_Option2Links";
            this.tB_Option2Links.Size = new System.Drawing.Size(65, 20);
            this.tB_Option2Links.TabIndex = 44;
            this.tB_Option2Links.Text = "NaN";
            // 
            // tB_Option3Links
            // 
            this.tB_Option3Links.Location = new System.Drawing.Point(397, 396);
            this.tB_Option3Links.Name = "tB_Option3Links";
            this.tB_Option3Links.Size = new System.Drawing.Size(65, 20);
            this.tB_Option3Links.TabIndex = 45;
            this.tB_Option3Links.Text = "NaN";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(632, 286);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(102, 23);
            this.button3.TabIndex = 47;
            this.button3.Text = "Copy To Clipboard";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(539, 286);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 48;
            this.button4.Text = "Remove";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 429);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.tB_Option3Links);
            this.Controls.Add(this.tB_Option2Links);
            this.Controls.Add(this.tB_Option1Links);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tB_Console);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.b_Update);
            this.Controls.Add(this.b_SaveNode);
            this.Controls.Add(this.lB_Nodes);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.nUD_Option3Mod);
            this.Controls.Add(this.nUD_Option2Mod);
            this.Controls.Add(this.nUD_Option1Mod);
            this.Controls.Add(this.textBox26);
            this.Controls.Add(this.textBox25);
            this.Controls.Add(this.textBox24);
            this.Controls.Add(this.tB_TalkingTo);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.tB_Option3);
            this.Controls.Add(this.tB_Option2);
            this.Controls.Add(this.tB_Option1);
            this.Controls.Add(this.tB_NodeKey);
            this.Controls.Add(this.tB_MainText);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nUD_Option1Mod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_Option2Mod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_Option3Mod)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tB_MainText;
        private System.Windows.Forms.TextBox tB_NodeKey;
        private System.Windows.Forms.TextBox tB_Option1;
        private System.Windows.Forms.TextBox tB_Option2;
        private System.Windows.Forms.TextBox tB_Option3;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox tB_TalkingTo;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.NumericUpDown nUD_Option1Mod;
        private System.Windows.Forms.NumericUpDown nUD_Option2Mod;
        private System.Windows.Forms.NumericUpDown nUD_Option3Mod;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mainToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createNewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem savingLoadingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadAllToolStripMenuItem;
        private System.Windows.Forms.ListBox lB_Nodes;
        private System.Windows.Forms.Button b_SaveNode;
        private System.Windows.Forms.Button b_Update;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tB_Console;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox tB_Option1Links;
        private System.Windows.Forms.TextBox tB_Option2Links;
        private System.Windows.Forms.TextBox tB_Option3Links;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}

