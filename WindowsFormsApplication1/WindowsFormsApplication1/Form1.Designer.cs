﻿namespace WindowsFormsApplication1 {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.TB_firstname = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.nUD_Age = new System.Windows.Forms.NumericUpDown();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.TB_LastName = new System.Windows.Forms.TextBox();
            this.rTB_Backstory = new System.Windows.Forms.RichTextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iB_NeutralImg = new System.Windows.Forms.PictureBox();
            this.iB_NegitiveImg = new System.Windows.Forms.PictureBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.nUD_Likeness = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.iB_PositiveImg = new System.Windows.Forms.PictureBox();
            this.tB_location = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.tB_PositiveName = new System.Windows.Forms.TextBox();
            this.tB_NeutralName = new System.Windows.Forms.TextBox();
            this.tB_NegitiveName = new System.Windows.Forms.TextBox();
            this.rTB_Traits = new System.Windows.Forms.RichTextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_Age)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iB_NeutralImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iB_NegitiveImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_Likeness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iB_PositiveImg)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(12, 33);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(56, 13);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "First name";
            // 
            // TB_firstname
            // 
            this.TB_firstname.Location = new System.Drawing.Point(12, 52);
            this.TB_firstname.Name = "TB_firstname";
            this.TB_firstname.Size = new System.Drawing.Size(67, 20);
            this.TB_firstname.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Location = new System.Drawing.Point(12, 78);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(32, 13);
            this.textBox3.TabIndex = 2;
            this.textBox3.Text = "Age";
            // 
            // nUD_Age
            // 
            this.nUD_Age.Location = new System.Drawing.Point(12, 94);
            this.nUD_Age.Name = "nUD_Age";
            this.nUD_Age.Size = new System.Drawing.Size(53, 20);
            this.nUD_Age.TabIndex = 3;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Location = new System.Drawing.Point(12, 136);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(56, 13);
            this.textBox4.TabIndex = 5;
            this.textBox4.Text = "Backstory";
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Location = new System.Drawing.Point(95, 33);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(56, 13);
            this.textBox5.TabIndex = 6;
            this.textBox5.Text = "Last name";
            // 
            // TB_LastName
            // 
            this.TB_LastName.Location = new System.Drawing.Point(95, 52);
            this.TB_LastName.Name = "TB_LastName";
            this.TB_LastName.Size = new System.Drawing.Size(67, 20);
            this.TB_LastName.TabIndex = 7;
            // 
            // rTB_Backstory
            // 
            this.rTB_Backstory.Location = new System.Drawing.Point(12, 155);
            this.rTB_Backstory.Name = "rTB_Backstory";
            this.rTB_Backstory.Size = new System.Drawing.Size(224, 140);
            this.rTB_Backstory.TabIndex = 8;
            this.rTB_Backstory.Text = "";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(95, 78);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(77, 13);
            this.textBox2.TabIndex = 9;
            this.textBox2.Text = "Base Likeness";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(852, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem1,
            this.loadToolStripMenuItem});
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.testToolStripMenuItem.Text = "Home";
            // 
            // saveToolStripMenuItem1
            // 
            this.saveToolStripMenuItem1.Name = "saveToolStripMenuItem1";
            this.saveToolStripMenuItem1.Size = new System.Drawing.Size(100, 22);
            this.saveToolStripMenuItem1.Text = "Save";
            this.saveToolStripMenuItem1.Click += new System.EventHandler(this.saveToolStripMenuItem1_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // iB_NeutralImg
            // 
            this.iB_NeutralImg.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.iB_NeutralImg.Location = new System.Drawing.Point(744, 42);
            this.iB_NeutralImg.Name = "iB_NeutralImg";
            this.iB_NeutralImg.Size = new System.Drawing.Size(96, 110);
            this.iB_NeutralImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.iB_NeutralImg.TabIndex = 11;
            this.iB_NeutralImg.TabStop = false;
            // 
            // iB_NegitiveImg
            // 
            this.iB_NegitiveImg.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.iB_NegitiveImg.Location = new System.Drawing.Point(744, 185);
            this.iB_NegitiveImg.Name = "iB_NegitiveImg";
            this.iB_NegitiveImg.Size = new System.Drawing.Size(96, 110);
            this.iB_NegitiveImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.iB_NegitiveImg.TabIndex = 12;
            this.iB_NegitiveImg.TabStop = false;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.Location = new System.Drawing.Point(668, 42);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(56, 13);
            this.textBox6.TabIndex = 13;
            this.textBox6.Text = "Neutral";
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.Location = new System.Drawing.Point(668, 185);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(56, 13);
            this.textBox7.TabIndex = 14;
            this.textBox7.Text = "Negitive";
            // 
            // nUD_Likeness
            // 
            this.nUD_Likeness.Location = new System.Drawing.Point(95, 94);
            this.nUD_Likeness.Name = "nUD_Likeness";
            this.nUD_Likeness.Size = new System.Drawing.Size(67, 20);
            this.nUD_Likeness.TabIndex = 15;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(663, 61);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(663, 90);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 17;
            this.button2.Text = "Remove";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(663, 204);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 18;
            this.button3.Text = "Add";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(663, 233);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 19;
            this.button4.Text = "Remove";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(466, 90);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 23;
            this.button5.Text = "Remove";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(466, 61);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 22;
            this.button6.Text = "Add";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox8.Location = new System.Drawing.Point(471, 42);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(56, 13);
            this.textBox8.TabIndex = 21;
            this.textBox8.Text = "Positive";
            // 
            // iB_PositiveImg
            // 
            this.iB_PositiveImg.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.iB_PositiveImg.Location = new System.Drawing.Point(547, 42);
            this.iB_PositiveImg.Name = "iB_PositiveImg";
            this.iB_PositiveImg.Size = new System.Drawing.Size(96, 110);
            this.iB_PositiveImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.iB_PositiveImg.TabIndex = 20;
            this.iB_PositiveImg.TabStop = false;
            // 
            // tB_location
            // 
            this.tB_location.Location = new System.Drawing.Point(189, 51);
            this.tB_location.Name = "tB_location";
            this.tB_location.Size = new System.Drawing.Size(190, 20);
            this.tB_location.TabIndex = 25;
            this.tB_location.TextChanged += new System.EventHandler(this.tB_location_TextChanged);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(385, 51);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 21);
            this.button7.TabIndex = 26;
            this.button7.Text = "Set location";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox10.Location = new System.Drawing.Point(189, 35);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(106, 13);
            this.textBox10.TabIndex = 27;
            this.textBox10.Text = "Resource Location";
            // 
            // tB_PositiveName
            // 
            this.tB_PositiveName.Location = new System.Drawing.Point(466, 119);
            this.tB_PositiveName.Name = "tB_PositiveName";
            this.tB_PositiveName.Size = new System.Drawing.Size(75, 20);
            this.tB_PositiveName.TabIndex = 29;
            this.tB_PositiveName.Text = "NaN";
            // 
            // tB_NeutralName
            // 
            this.tB_NeutralName.BackColor = System.Drawing.SystemColors.Window;
            this.tB_NeutralName.Location = new System.Drawing.Point(663, 119);
            this.tB_NeutralName.Name = "tB_NeutralName";
            this.tB_NeutralName.ReadOnly = true;
            this.tB_NeutralName.Size = new System.Drawing.Size(75, 20);
            this.tB_NeutralName.TabIndex = 30;
            this.tB_NeutralName.Text = "NaN";
            // 
            // tB_NegitiveName
            // 
            this.tB_NegitiveName.BackColor = System.Drawing.SystemColors.Window;
            this.tB_NegitiveName.Location = new System.Drawing.Point(663, 262);
            this.tB_NegitiveName.Name = "tB_NegitiveName";
            this.tB_NegitiveName.ReadOnly = true;
            this.tB_NegitiveName.Size = new System.Drawing.Size(75, 20);
            this.tB_NegitiveName.TabIndex = 31;
            this.tB_NegitiveName.Text = "NaN";
            // 
            // rTB_Traits
            // 
            this.rTB_Traits.Location = new System.Drawing.Point(242, 155);
            this.rTB_Traits.Name = "rTB_Traits";
            this.rTB_Traits.Size = new System.Drawing.Size(218, 72);
            this.rTB_Traits.TabIndex = 32;
            this.rTB_Traits.Text = "";
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox9.Location = new System.Drawing.Point(242, 136);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(77, 13);
            this.textBox9.TabIndex = 33;
            this.textBox9.Text = "Traits";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 316);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.rTB_Traits);
            this.Controls.Add(this.tB_NegitiveName);
            this.Controls.Add(this.tB_NeutralName);
            this.Controls.Add(this.tB_PositiveName);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.tB_location);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.iB_PositiveImg);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.nUD_Likeness);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.iB_NegitiveImg);
            this.Controls.Add(this.iB_NeutralImg);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.rTB_Backstory);
            this.Controls.Add(this.TB_LastName);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.nUD_Age);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.TB_firstname);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nUD_Age)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iB_NeutralImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iB_NegitiveImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_Likeness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iB_PositiveImg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox TB_firstname;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.NumericUpDown nUD_Age;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox TB_LastName;
        private System.Windows.Forms.RichTextBox rTB_Backstory;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.PictureBox iB_NeutralImg;
        private System.Windows.Forms.PictureBox iB_NegitiveImg;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.NumericUpDown nUD_Likeness;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.PictureBox iB_PositiveImg;
        private System.Windows.Forms.TextBox tB_location;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox tB_PositiveName;
        private System.Windows.Forms.TextBox tB_NeutralName;
        private System.Windows.Forms.TextBox tB_NegitiveName;
        private System.Windows.Forms.RichTextBox rTB_Traits;
        private System.Windows.Forms.TextBox textBox9;
    }
}

