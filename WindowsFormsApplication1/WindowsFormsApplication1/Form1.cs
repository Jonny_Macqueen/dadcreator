﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;







namespace WindowsFormsApplication1 {
    public partial class Form1 : Form {

        DataTable dadTable = new DataTable("Dad");

        //string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
        string path = AppDomain.CurrentDomain.BaseDirectory;
        string directory;


        public Image positiveImg {
            get { return iB_PositiveImg.Image; }
            set {
                iB_PositiveImg.Image = value;
                this.Refresh();
            }
        }
        public Image netrualImg {
            get { return iB_NeutralImg.Image; }
            set {
                iB_NeutralImg.Image = value;
                this.Refresh();
            }
        }
        public Image negitiveImg {
            get { return iB_NegitiveImg.Image; }
            set {
                iB_NegitiveImg.Image = value;
                this.Refresh();
            }
        }



        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            directory = System.IO.Path.GetDirectoryName(path) + "\\Resources";


            dadTable.Columns.Add("firstName", typeof(string));
            dadTable.Columns.Add("LastName", typeof(string));
            dadTable.Columns.Add("Age", typeof(string));
            dadTable.Columns.Add("Likeness", typeof(string));
            dadTable.Columns.Add("Backstory", typeof(string));
            dadTable.Columns.Add("Traits", typeof(string));
            dadTable.Columns.Add("positiveImg", typeof(string));
            dadTable.Columns.Add("netrualImg", typeof(string));
            dadTable.Columns.Add("negitiveImg", typeof(string));

            tB_location.Text = directory;
        }
        private void saveToolStripMenuItem1_Click(object sender, EventArgs e) {

            dadTable.Clear();

            dadTable.Rows.Add(
                TB_firstname.Text,
                TB_LastName.Text,
                nUD_Age.Value,
                nUD_Likeness.Value,
                rTB_Backstory.Text,
                rTB_Traits.Text,
                tB_PositiveName.Text,
                tB_NeutralName.Text,
                tB_NegitiveName.Text
                );
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "XML|*.xml";
            sfd.InitialDirectory = directory;
            sfd.RestoreDirectory = true;
            sfd.AutoUpgradeEnabled = false;


                if (sfd.ShowDialog() == DialogResult.OK) {
                    dadTable.WriteXml(sfd.FileName);
                }
            
        }


        private void loadToolStripMenuItem_Click(object sender, EventArgs e) {
            var confirmResult = MessageBox.Show("Creating a new grid will wipe the current one" + "\n" + "Are you sure?",
                             "Create New Grid",
             MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes) {
                OpenFileDialog lfd = new OpenFileDialog();
                lfd.Filter = "XML|*.xml";
                if (lfd.ShowDialog() == DialogResult.OK) {
                    XmlReader xmlFile = XmlReader.Create(lfd.FileName, new XmlReaderSettings());
                    dadTable.Clear();
                    dadTable.ReadXml(xmlFile);


                    TB_firstname.Text = dadTable.Rows[0]["firstName"].ToString();
                    TB_LastName.Text = dadTable.Rows[0]["LastName"].ToString();
                    nUD_Age.Value = Int32.Parse(dadTable.Rows[0]["Age"].ToString());
                    nUD_Likeness.Value = Int32.Parse(dadTable.Rows[0]["Likeness"].ToString());
                    rTB_Backstory.Text = dadTable.Rows[0]["Backstory"].ToString();
                    this.Refresh();
                    this.Show();


                }
            }
            else {
                // If 'No', do something here.
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e) {

            using (OpenFileDialog dlg = new OpenFileDialog()) {
                dlg.Title = "Open Image";
                dlg.Filter = "All Graphics Types|*.bmp;*.jpg;*.jpeg;*.png;*.tif;*.tiff|"
                +"BMP|*.bmp|GIF|*.gif|JPG|*.jpg;*.jpeg|PNG|*.png|TIFF|*.tif;*.tiff|"
                + "Zip Files|*.zip;*.rar";


                dlg.InitialDirectory = directory;
                dlg.RestoreDirectory = true;
                dlg.AutoUpgradeEnabled = false;


                if (dlg.ShowDialog() == DialogResult.OK) {
                    netrualImg = new Bitmap(dlg.FileName);
                    tB_NeutralName.Text = dlg.SafeFileName;
                }
            }
        }

        private void button7_Click(object sender, EventArgs e) {
            using (FolderBrowserDialog dlg = new FolderBrowserDialog()) {
                dlg.Description = "Select a folder";
                if (dlg.ShowDialog() == DialogResult.OK) {
                    tB_location.Text = dlg.SelectedPath;
                }
            }
        }

        private void tB_location_TextChanged(object sender, EventArgs e) {
            directory = tB_location.Text;

        }

        private void button6_Click(object sender, EventArgs e) {
            using (OpenFileDialog dlg = new OpenFileDialog()) {
                dlg.Title = "Open Image";
                dlg.Filter = "All Graphics Types|*.bmp;*.jpg;*.jpeg;*.png;*.tif;*.tiff|"
                + "BMP|*.bmp|GIF|*.gif|JPG|*.jpg;*.jpeg|PNG|*.png|TIFF|*.tif;*.tiff|"
                + "Zip Files|*.zip;*.rar";


                dlg.InitialDirectory = directory;
                dlg.RestoreDirectory = true;
                dlg.AutoUpgradeEnabled = false;


                if (dlg.ShowDialog() == DialogResult.OK) {
                    positiveImg = new Bitmap(dlg.FileName);
                    tB_PositiveName.Text = dlg.SafeFileName;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e) {
            tB_NeutralName.Text = "NaN";
            netrualImg = null;
            this.Refresh();
        }

        private void button5_Click(object sender, EventArgs e) {
            tB_PositiveName.Text = "NaN";
            positiveImg = null;
            this.Refresh();
        }

        private void button4_Click(object sender, EventArgs e) {
            tB_NegitiveName.Text = "NaN";
            negitiveImg = null;
            this.Refresh();
        }

        private void button3_Click(object sender, EventArgs e) {
            using (OpenFileDialog dlg = new OpenFileDialog()) {
                dlg.Title = "Open Image";
                dlg.Filter = "All Graphics Types|*.bmp;*.jpg;*.jpeg;*.png;*.tif;*.tiff|"
                + "BMP|*.bmp|GIF|*.gif|JPG|*.jpg;*.jpeg|PNG|*.png|TIFF|*.tif;*.tiff|"
                + "Zip Files|*.zip;*.rar";


                dlg.InitialDirectory = directory;
                dlg.RestoreDirectory = true;
                dlg.AutoUpgradeEnabled = false;


                if (dlg.ShowDialog() == DialogResult.OK) {
                    negitiveImg = new Bitmap(dlg.FileName);
                    tB_NegitiveName.Text = dlg.SafeFileName;
                }
            }
        }
    }
}
